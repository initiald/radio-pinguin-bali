class ModelBanner {
  List<Bannerlist> bannerlist;
  int success;

  ModelBanner({this.bannerlist, this.success});

  ModelBanner.fromJson(Map<String, dynamic> json) {
    if (json['bannerlist'] != null) {
      bannerlist = <Bannerlist>[];
      json['bannerlist'].forEach((v) {
        bannerlist.add(new Bannerlist.fromJson(v));
      });
    }
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.bannerlist != null) {
      data['bannerlist'] = this.bannerlist.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    return data;
  }
}

class Bannerlist {
  String image;
  String link;

  Bannerlist({this.image, this.link});

  Bannerlist.fromJson(Map<String, dynamic> json) {
    image = json['image'];
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    data['link'] = this.link;
    return data;
  }
}
