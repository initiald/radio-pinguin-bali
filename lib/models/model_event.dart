class ModelEvent {
  List<Newslist> newslist;
  int success;

  ModelEvent({this.newslist, this.success});

  ModelEvent.fromJson(Map<String, dynamic> json) {
    if (json['newslist'] != null) {
      newslist = <Newslist>[];
      json['newslist'].forEach((v) {
        newslist.add(new Newslist.fromJson(v));
      });
    }
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.newslist != null) {
      data['newslist'] = this.newslist.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    return data;
  }
}

class Newslist {
  String id;
  String tanggal;
  String judul;
  String gambar;
  String ringkasan;
  String isi;

  Newslist(
      {this.id,
      this.tanggal,
      this.judul,
      this.gambar,
      this.ringkasan,
      this.isi});

  Newslist.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tanggal = json['tanggal'];
    judul = json['judul'];
    gambar = json['gambar'];
    ringkasan = json['ringkasan'];
    isi = json['isi'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tanggal'] = this.tanggal;
    data['judul'] = this.judul;
    data['gambar'] = this.gambar;
    data['ringkasan'] = this.ringkasan;
    data['isi'] = this.isi;
    return data;
  }
}
