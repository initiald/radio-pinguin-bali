class ModelGalleryDetail {
  List<String> detail;
  int success;

  ModelGalleryDetail({this.detail, this.success});

  ModelGalleryDetail.fromJson(Map<String, dynamic> json) {
    detail = json['detail'].cast<String>();
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['detail'] = this.detail;
    data['success'] = this.success;
    return data;
  }
}
