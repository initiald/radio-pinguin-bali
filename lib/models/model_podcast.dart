class ModelPodcast {
  List<Podcast> podcast;
  int success;

  ModelPodcast({this.podcast, this.success});

  ModelPodcast.fromJson(Map<String, dynamic> json) {
    if (json['podcast'] != null) {
      podcast = <Podcast>[];
      json['podcast'].forEach((v) {
        podcast.add(new Podcast.fromJson(v));
      });
    }
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.podcast != null) {
      data['podcast'] = this.podcast.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    return data;
  }
}

class Podcast {
  String id;
  String tanggal;
  String nama;
  String gambar;
  String file;

  Podcast({this.id, this.tanggal, this.nama, this.gambar, this.file});

  Podcast.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tanggal = json['tanggal'];
    nama = json['nama'];
    gambar = json['gambar'];
    file = json['file'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tanggal'] = this.tanggal;
    data['nama'] = this.nama;
    data['gambar'] = this.gambar;
    data['file'] = this.file;
    return data;
  }
}
