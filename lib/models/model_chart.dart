class ModelChart {
  List<Chartlist> chartlist;
  int success;

  ModelChart({this.chartlist, this.success});

  ModelChart.fromJson(Map<String, dynamic> json) {
    if (json['chartlist'] != null) {
      chartlist = <Chartlist>[];
      json['chartlist'].forEach((v) {
        chartlist.add(new Chartlist.fromJson(v));
      });
    }
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.chartlist != null) {
      data['chartlist'] = this.chartlist.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    return data;
  }
}

class Chartlist {
  String id;
  String title;
  String periode;
  String tanggal;
  String file;
  String deskripsi;

  Chartlist(
      {this.id,
      this.title,
      this.periode,
      this.tanggal,
      this.file,
      this.deskripsi});

  Chartlist.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    periode = json['periode'];
    tanggal = json['tanggal'];
    file = json['file'];
    deskripsi = json['deskripsi'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['periode'] = this.periode;
    data['tanggal'] = this.tanggal;
    data['file'] = this.file;
    data['deskripsi'] = this.deskripsi;
    return data;
  }
}
