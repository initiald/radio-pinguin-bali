import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:radio_pinguin/models/get_prod.dart';
import 'package:radio_pinguin/models/model_banner.dart';
import 'package:radio_pinguin/models/model_chart.dart';
import 'package:radio_pinguin/models/model_chart_detail.dart';
import 'package:radio_pinguin/models/model_contact.dart';
import 'package:radio_pinguin/models/model_event.dart';
import 'package:radio_pinguin/models/model_gallery.dart';
import 'package:radio_pinguin/models/model_gallery_detail.dart';
import 'package:radio_pinguin/models/model_news.dart';
import 'package:radio_pinguin/models/model_podcast.dart';
import 'package:radio_pinguin/models/model_program.dart';
import 'package:radio_pinguin/models/model_stream_route.php.dart';

class servicewrapper {
  var api_radio = "https://radiopinguinfm.com/json_api/";
  var baseurl = "https://androidtutorial.blueappsoftware.com/";
  var apifolder = "webapi/";
// see complete url --
// http://androidtutorial.blueappsoftware.com/webapi/get_jsondata.php
  // advance usage
  Future<get_prod> getProdCall() async {
    var url = baseurl + apifolder + "get_jsondata.php";
    final body = {'language': 'default', 'securecode': '123'};
    final bodyjson = json.encode(body);
    // pass headers parameters if any
    final response = await http.post(url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8'
        },
        body: bodyjson);
    print(" url call from " + url);
    if (response.statusCode == 200) {
      print('url hit successful' + response.body);
      String data = response.body;
      print(' prod name - ' + jsonDecode(data)['Information'][0]['name']);
      return get_prod.fromJson(json.decode(response.body));
    } else {
      print('failed to get data');
      throw Exception('Failed to get data');
    }
  }

  Future<ModelStreamRoute> getStreamRoute() async {
    var url = api_radio + "stream_route.php";
    var response = await http.get(url);
    // print('Response status: ${response.statusCode}');
    // print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      return ModelStreamRoute.fromJson(json.decode(response.body));
    } else {
      print('failed to get data');
      throw Exception('Failed to get data');
    }
  }

  Future<ModelProgram> getProgram(String hari) async {
    var url = api_radio + "program.php";
    var vbody = {'hari': hari};
    var response = await http.post(url, body: vbody);
    // print('Response status: ${response.statusCode}');
    // print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      return ModelProgram.fromJson(json.decode(response.body));
    } else {
      print('failed to get data');
      throw Exception('Failed to get data');
    }
  }

  Future<ModelChartDetail> getChartDetail(String id) async {
    print(id);
    var url = api_radio + "chartdetail.php";
    var vbody = {'id': id};
    var response = await http.post(url, body: vbody);
    // print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      return ModelChartDetail.fromJson(json.decode(response.body));
    } else {
      print('failed to get data');
      throw Exception('Failed to get data');
    }
  }

  Future<ModelGalleryDetail> getGalleryDetail(String id) async {
    var url = api_radio + "gallerydetail.php";
    var vbody = {'id': id};
    var response = await http.post(url, body: vbody);
    // print('Response status: ${response.statusCode}');
    // print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      return ModelGalleryDetail.fromJson(json.decode(response.body));
    } else {
      print('failed to get data');
      throw Exception('Failed to get data');
    }
  }

  Future<ModelContact> getContact() async {
    var response = await http.get(api_radio + "default.php");
    // print('Response status: ${response.statusCode}');
    // print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      return ModelContact.fromJson(json.decode(response.body));
    } else {
      print('failed to get data');
      throw Exception('Failed to get data');
    }
  }

  Future<ModelChart> getChart() async {
    var response = await http.get(api_radio + "chart.php");
    // print('Response status: ${response.statusCode}');
    // print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      return ModelChart.fromJson(json.decode(response.body));
      Map<String, dynamic> map = json.decode(response.body);
      List<dynamic> data = map["chartlist"];
      print(data[0]["title"]);
    } else {
      print('failed to get data');
      throw Exception('Failed to get data');
    }
  }

  Future<ModelGallery> getGallery() async {
    var response = await http.get(api_radio + "gallery.php");
    // print('Response status: ${response.statusCode}');
    // print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      return ModelGallery.fromJson(json.decode(response.body));
    } else {
      print('failed to get data');
      throw Exception('Failed to get data');
    }
  }

  Future<ModelBanner> getBanner() async {
    var response = await http.get(api_radio + "banner.php");
    // print('Response status: ${response.statusCode}');
    // print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      return ModelBanner.fromJson(json.decode(response.body));
    } else {
      print('failed to get data');
      throw Exception('Failed to get data');
    }
  }

  Future<ModelEvent> getEvent() async {
    var response = await http.get(api_radio + "events.php");
    // print('Response status: ${response.statusCode}');
    // print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      return ModelEvent.fromJson(json.decode(response.body));
    } else {
      print('failed to get data');
      throw Exception('Failed to get data');
    }
  }

  Future<ModelNews> getNews() async {
    var response = await http.get(api_radio + "news.php");
    // print('Response status: ${response.statusCode}');
    // print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      return ModelNews.fromJson(json.decode(response.body));
    } else {
      print('failed to get data');
      throw Exception('Failed to get data');
    }
  }

  // simple usage
  Future<ModelPodcast> getPodcast() async {
    var response = await http.get(api_radio + "podcast.php");
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      print('url hit successful' + response.body);
      String data = response.body;
      // print(' prod name - ' + jsonDecode(data)['Information'][0]['name']);
      print(' res - ' + jsonDecode(data));
      return ModelPodcast.fromJson(json.decode(response.body));
    } else {
      print('failed to get data');
      throw Exception('Failed to get data');
    }

    // print(await http.read('https://example.com/foobar.txt'));
  }
}
