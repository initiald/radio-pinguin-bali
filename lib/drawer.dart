import 'package:flutter/material.dart';
import 'package:radio_pinguin/constants.dart';
import 'package:radio_pinguin/views/contact.dart';
import 'package:radio_pinguin/views/event.dart';
import 'package:radio_pinguin/views/gallery.dart';
import 'package:radio_pinguin/views/live_chart.dart';
import 'package:radio_pinguin/views/news.dart';
import 'package:radio_pinguin/views/player.dart';

Drawer drawer(context) {
  return Drawer(
    child: Container(
      color: Color.fromRGBO(41, 121, 255, 1.0),
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          Container(
            color: Color.fromRGBO(41, 98, 255, 1.0),
            height: 150,
            child: DrawerHeader(
              child: Row(
                children: [
                  Image.network(
                    "https://radiopinguinfm.com/images/logo.png",
                    fit: BoxFit.contain,
                    height: 60,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "MENU",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          "SELECTOR",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: List.generate(
                menus.length,
                (index) => ListTile(
                  title: Row(
                    children: [
                      Image.asset(
                        "images/btn_play.png",
                        fit: BoxFit.contain,
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          menus[index],
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    // Update the state of the app.
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          Widget ret;
                          switch (index) {
                            case 0:
                              ret = Player();
                              break; // The switch statement must be told to exit, or it will execute every case.
                            case 1:
                              ret = LiveChart();
                              break;
                            case 2:
                              ret = Contact();
                              break;
                            case 3:
                              ret = Gallery();
                              break;
                            case 4:
                              ret = Event();
                              break;
                            case 5:
                              ret = News();
                              break;
                            default:
                              ret = Player();
                          }
                          return ret;
                        },
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
