import 'package:flutter/material.dart';
import 'package:flutter_radio/flutter_radio.dart';
import 'package:radio_pinguin/app_bar.dart';
import 'package:radio_pinguin/drawer.dart';
import 'package:radio_pinguin/models/model_gallery.dart';
import 'package:radio_pinguin/services.dart';
import 'package:radio_pinguin/views/gallery_detail.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Gallery extends StatefulWidget {
  Gallery({Key key}) : super(key: key);
  @override
  _Gallery createState() => _Gallery();
}

class _Gallery extends State<Gallery> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  Future<ModelGallery> getModelGallery;

  final globalKey = GlobalKey<ScaffoldState>();
  bool isPlaying = false;
  String streamUrl;

  void initState() {
    super.initState();
    getModelGallery = _getData();

    _prefs.then((SharedPreferences prefs) {
      streamUrl = prefs.getString('streamUrl');
      print('init is playing $isPlaying');
      setState(() {
        // isPlaying = prefs.getInt('isPlaying');
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: globalKey,
      appBar: appBar(),
      body: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(41, 121, 255, 1.0),
        ),
        child: FutureBuilder<ModelGallery>(
          future: getModelGallery,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var data = snapshot.data.gallery;
              // print(data);
              return Container(
                // height: 200,
                child: GridView.count(
                  scrollDirection: Axis.vertical,
                  crossAxisCount: 2,
                  children: List.generate(data.length, (index) {
                    return Card(
                      // color: Colors.amber,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return GalleryDetail(idx: data[index].id);
                              },
                            ),
                          );
                          // print('gallery clicked ${index}');
                        },
                        child: Column(
                          children: [
                            Expanded(
                              child: FadeInImage.assetNetwork(
                                image:
                                    "https://radiopinguinfm.com/images/photos/${data[index].photo}",
                                placeholder:
                                    "images/logo.png", //new Image.asset("images/logo.png"),
                                fit: BoxFit.contain,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                data[index].title,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
                ),
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            // By default show a loading spinner.
            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        // color: Colors.white,
        child: Container(
          decoration: BoxDecoration(
            color: Color.fromRGBO(41, 98, 255, 1.0),
          ),
          // padding: EdgeInsets.all(5),
          child: SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: GestureDetector(
                    onTap: () {
                      // Scaffold.of(context).openDrawer();
                      // final snackBar = SnackBar(content: Text('Profile saved'));
                      globalKey.currentState.openDrawer();
                      print("btn menu clicked");
                    },
                    child: Image.asset(
                      "images/btn_menu.png",
                      fit: BoxFit.contain,
                      height: 35,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: GestureDetector(
                    onTap: () {
                      // get data and assign to model class
                      playingStatus();
                      FlutterRadio.playOrPause(url: streamUrl);
                      // playingStatus();
                      // print("btn play clicked ${streamUrl}");
                    },
                    child: Image.asset(
                      // "images/btn_play.png",
                      !isPlaying
                          ? "images/btn_play.png"
                          : "images/btn_pause.png",
                      fit: BoxFit.contain,
                      height: 55,
                    ),
                  ),
                  // child: Container(),
                ),
              ],
            ),
          ),
        ),
      ),
      drawer: drawer(context),
    );
  }

  Future<ModelGallery> _getData() {
    print(" get data using http");
    servicewrapper wrapper = new servicewrapper();
    // print(wrapper.getChart());
    return wrapper.getGallery();
  }

  Future playingStatus() async {
    bool isP = await FlutterRadio.isPlaying();
    setState(() {
      isPlaying = isP;
      print('is playing $isPlaying');
    });
  }
}
