import 'package:flutter/material.dart';
import 'package:flutter_radio/flutter_radio.dart';
import 'package:radio_pinguin/app_bar.dart';
import 'package:radio_pinguin/drawer.dart';
import 'package:radio_pinguin/models/model_chart_detail.dart';
import 'package:radio_pinguin/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LiveChartDetail extends StatefulWidget {
  String idx;
  // bool isPlaying;
  LiveChartDetail({Key key, this.idx}) : super(key: key);
  @override
  _LiveChartDetail createState() => _LiveChartDetail();
}

class _LiveChartDetail extends State<LiveChartDetail> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  Future<ModelChartDetail> getModelChartDetail;

  final globalKey = GlobalKey<ScaffoldState>();
  bool isPlaying = false;
  String streamUrl;

  void initState() {
    super.initState();
    // isPlaying = widget.isPlaying;
    getModelChartDetail = _getData();

    _prefs.then((SharedPreferences prefs) {
      streamUrl = prefs.getString('streamUrl');
      print('init is playing $isPlaying');
      setState(() {
        // isPlaying = prefs.getInt('isPlaying');
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: globalKey,
      appBar: appBar(),
      body: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(41, 121, 255, 1.0),
        ),
        child: FutureBuilder<ModelChartDetail>(
          future: getModelChartDetail,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var data = snapshot.data.chartlistdetail;
              // List<ModelChart> data = snapshot.data;
              // print(data);
              return ListView.separated(
                padding: const EdgeInsets.all(8),
                itemCount: data.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    decoration: BoxDecoration(
                      color: Colors.white60,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  data[index].rangking,
                                  style: Theme.of(context).textTheme.headline3,
                                ),
                              ),
                            ],
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  data[index].judul,
                                  style: Theme.of(context).textTheme.headline3,
                                ),
                                Text(
                                  data[index].penyanyi,
                                )
                                // Text(DateFormat('dd-MM-yyyy')
                                //     .format(DateTime.parse(data[index].tanggal))),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            // By default show a loading spinner.
            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        // color: Colors.white,
        child: Container(
          decoration: BoxDecoration(
            color: Color.fromRGBO(41, 98, 255, 1.0),
          ),
          // padding: EdgeInsets.all(5),
          child: SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: GestureDetector(
                    onTap: () {
                      // Scaffold.of(context).openDrawer();
                      // final snackBar = SnackBar(content: Text('Profile saved'));
                      globalKey.currentState.openDrawer();
                      print("btn menu clicked");
                    },
                    child: Image.asset(
                      "images/btn_menu.png",
                      fit: BoxFit.contain,
                      height: 35,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: GestureDetector(
                    onTap: () {
                      // get data and assign to model class
                      playingStatus();
                      FlutterRadio.playOrPause(url: streamUrl);
                      // playingStatus();
                      // print("btn play clicked ${streamUrl}");
                    },
                    child: Image.asset(
                      // "images/btn_play.png",
                      !isPlaying
                          ? "images/btn_play.png"
                          : "images/btn_pause.png",
                      fit: BoxFit.contain,
                      height: 55,
                    ),
                  ),
                  // child: Container(),
                ),
              ],
            ),
          ),
        ),
      ),
      drawer: drawer(context),
    );
  }

  Future<ModelChartDetail> _getData() {
    print(" get data using http");
    servicewrapper wrapper = new servicewrapper();
    // print(wrapper.getChart());
    return wrapper.getChartDetail(widget.idx);
  }

  Future playingStatus() async {
    bool isP = await FlutterRadio.isPlaying();
    setState(() {
      isPlaying = isP;
      print('is playing $isPlaying');
    });
  }
}
