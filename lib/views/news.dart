import 'package:flutter/material.dart';
import 'package:flutter_radio/flutter_radio.dart';
import 'package:radio_pinguin/app_bar.dart';
import 'package:radio_pinguin/drawer.dart';
import 'package:radio_pinguin/models/model_news.dart';
import 'package:radio_pinguin/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class News extends StatefulWidget {
  News({Key key}) : super(key: key);
  @override
  _News createState() => _News();
}

class _News extends State<News> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  Future<ModelNews> getModelNews;

  final globalKey = GlobalKey<ScaffoldState>();
  bool isPlaying = false;
  String streamUrl;

  void initState() {
    super.initState();
    getModelNews = _getData();

    _prefs.then((SharedPreferences prefs) {
      streamUrl = prefs.getString('streamUrl');
      print('init is playing $isPlaying');
      setState(() {
        // isPlaying = prefs.getInt('isPlaying');
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: globalKey,
      appBar: appBar(),
      body: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(41, 121, 255, 1.0),
        ),
        child: FutureBuilder<ModelNews>(
          future: getModelNews,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var data = snapshot.data.newslist;
              // print(data);
              return ListView.separated(
                padding: const EdgeInsets.all(8),
                itemCount: data.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    decoration: BoxDecoration(
                      color: Colors.white60,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                        onTap: () {
                          _launchURL(
                              'http://radiopinguinfm.com/detail_news.asp?id=' +
                                  data[index].id);
                          // print('gallery clicked ${index}');
                        },
                        child: Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: FadeInImage.assetNetwork(
                                  image:
                                      "https://radiopinguinfm.com/images/news/${data[index].gambar}",
                                  placeholder:
                                      "images/logo.png", //new Image.asset("images/logo.png"),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    data[index].judul,
                                    style:
                                        Theme.of(context).textTheme.headline3,
                                  ),
                                  Text(
                                    data[index].ringkasan == null
                                        ? ''
                                        : data[index].ringkasan,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  Text(
                                    data[index].tanggal,
                                  )
                                  // Text(DateFormat('dd-MM-yyyy')
                                  //     .format(DateTime.parse(data[index].tanggal))),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            // By default show a loading spinner.
            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        // color: Colors.white,
        child: Container(
          decoration: BoxDecoration(
            color: Color.fromRGBO(41, 98, 255, 1.0),
          ),
          // padding: EdgeInsets.all(5),
          child: SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: GestureDetector(
                    onTap: () {
                      // Scaffold.of(context).openDrawer();
                      // final snackBar = SnackBar(content: Text('Profile saved'));
                      globalKey.currentState.openDrawer();
                      print("btn menu clicked");
                    },
                    child: Image.asset(
                      "images/btn_menu.png",
                      fit: BoxFit.contain,
                      height: 35,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: GestureDetector(
                    onTap: () {
                      // get data and assign to model class
                      playingStatus();
                      FlutterRadio.playOrPause(url: streamUrl);
                      // playingStatus();
                      // print("btn play clicked ${streamUrl}");
                    },
                    child: Image.asset(
                      // "images/btn_play.png",
                      !isPlaying
                          ? "images/btn_play.png"
                          : "images/btn_pause.png",
                      fit: BoxFit.contain,
                      height: 55,
                    ),
                  ),
                  // child: Container(),
                ),
              ],
            ),
          ),
        ),
      ),
      drawer: drawer(context),
    );
  }

  Future<ModelNews> _getData() {
    print(" get data using http");
    servicewrapper wrapper = new servicewrapper();
    // print(wrapper.getChart());
    return wrapper.getNews();
  }

  Future playingStatus() async {
    bool isP = await FlutterRadio.isPlaying();
    setState(() {
      isPlaying = isP;
      print('is playing $isPlaying');
    });
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceWebView: true);
    } else {
      throw 'Could not launch $url';
    }
  }
}
