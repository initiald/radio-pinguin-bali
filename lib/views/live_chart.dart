import 'package:flutter/material.dart';
import 'package:flutter_radio/flutter_radio.dart';
import 'package:intl/intl.dart';
import 'package:radio_pinguin/app_bar.dart';
import 'package:radio_pinguin/drawer.dart';
import 'package:radio_pinguin/models/model_chart.dart';
import 'package:radio_pinguin/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'live_chart_detail.dart';

class LiveChart extends StatefulWidget {
  LiveChart({Key key}) : super(key: key);
  @override
  _LiveChart createState() => _LiveChart();
}

class _LiveChart extends State<LiveChart> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  Future<ModelChart> getModelChart;

  // static const streamUrl = "https://live.nagaswarafm.com:9900/stream";
  final globalKey = GlobalKey<ScaffoldState>();
  bool isPlaying = false;
  String streamUrl;

  void initState() {
    super.initState();
    getModelChart = _getData();

    _prefs.then((SharedPreferences prefs) {
      streamUrl = prefs.getString('streamUrl');
      print('init is playing $isPlaying');
      setState(() {
        // isPlaying = prefs.getInt('isPlaying');
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: globalKey,
      appBar: appBar(),
      body: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(41, 121, 255, 1.0),
        ),
        child: FutureBuilder<ModelChart>(
          future: getModelChart,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var data = snapshot.data.chartlist;
              // List<ModelChart> data = snapshot.data;
              // print(data);
              return ListView.separated(
                padding: const EdgeInsets.all(8),
                itemCount: data.length,
                itemBuilder: (BuildContext context, int index) {
                  // print(data[index]);
                  return Container(
                    decoration: BoxDecoration(
                      color: Colors.white60,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return LiveChartDetail(idx: data[index].id);
                              },
                            ),
                          );
                          // print('chart clicked ${index}');
                          // Scaffold.of(context).openDrawer();
                          // final snackBar = SnackBar(content: Text('Profile saved'));
                          // globalKey.currentState.openDrawer();
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  data[index].title,
                                  style: Theme.of(context).textTheme.headline3,
                                ),
                                Text(
                                  DateFormat('dd-MM-yyyy').format(
                                      DateTime.parse(data[index].tanggal)),
                                  style: Theme.of(context).textTheme.headline3,
                                ),
                              ],
                            ),
                            Text(
                              data[index].deskripsi,
                            )
                            // Text(DateFormat('dd-MM-yyyy')
                            //     .format(DateTime.parse(data[index].tanggal))),
                          ],
                        ),
                      ),
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            // By default show a loading spinner.
            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        // color: Colors.white,
        child: Container(
          decoration: BoxDecoration(
            color: Color.fromRGBO(41, 98, 255, 1.0),
          ),
          // padding: EdgeInsets.all(5),
          child: SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: GestureDetector(
                    onTap: () {
                      // Scaffold.of(context).openDrawer();
                      // final snackBar = SnackBar(content: Text('Profile saved'));
                      globalKey.currentState.openDrawer();
                      print("btn menu clicked");
                    },
                    child: Image.asset(
                      "images/btn_menu.png",
                      fit: BoxFit.contain,
                      height: 35,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: GestureDetector(
                    onTap: () {
                      // get data and assign to model class
                      playingStatus();
                      FlutterRadio.playOrPause(url: streamUrl);
                      // playingStatus();
                      // print("btn play clicked ${streamUrl}");
                    },
                    child: Image.asset(
                      // "images/btn_play.png",
                      !isPlaying
                          ? "images/btn_play.png"
                          : "images/btn_pause.png",
                      fit: BoxFit.contain,
                      height: 55,
                    ),
                  ),
                  // child: Container(),
                ),
              ],
            ),
          ),
        ),
      ),
      drawer: drawer(context),
    );
  }

  Future<ModelChart> _getData() {
    print(" get data using http");
    servicewrapper wrapper = new servicewrapper();
    // print(wrapper.getChart());
    return wrapper.getChart();
  }

  Future playingStatus() async {
    // final SharedPreferences prefs = await _prefs;
    // prefs.setInt("isPlaying", isPlaying == 0 ? 1 : 0);

    bool isP = await FlutterRadio.isPlaying();
    print('is playing 1 $isPlaying');
    setState(() {
      isPlaying = isP;
      print('is playing 2 $isPlaying');
    });
  }
}
