import 'package:flutter/material.dart';
import 'package:flutter_radio/flutter_radio.dart';
import 'package:radio_pinguin/app_bar.dart';
import 'package:radio_pinguin/drawer.dart';
import 'package:radio_pinguin/models/model_contact.dart';
import 'package:radio_pinguin/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class Contact extends StatefulWidget {
  Contact({Key key}) : super(key: key);
  @override
  _Contact createState() => _Contact();
}

class _Contact extends State<Contact> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  Future<ModelContact> getModelContact;

  final globalKey = GlobalKey<ScaffoldState>();
  bool isPlaying = false;
  String streamUrl;

  void initState() {
    super.initState();
    getModelContact = _getData();
    // print("initState ${getModelContact}");
    _prefs.then((SharedPreferences prefs) {
      streamUrl = prefs.getString('streamUrl');
      print('init is playing $isPlaying');
      setState(() {
        // isPlaying = prefs.getInt('isPlaying');
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: globalKey,
      appBar: appBar(),
      body: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(41, 121, 255, 1.0),
        ),
        // child: FutureBuilder<ModelContact>(
        //   future: getModelContact,
        //   builder: (context, snapshot) {
        //     if (snapshot.hasData) {
        //       var data = snapshot.data.contact;
        //       return Text("true ${data}");
        //     } else {
        //       return Text("danang ${snapshot}");
        //     }
        //   },
        // ),
        child: FutureBuilder<ModelContact>(
          future: getModelContact,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var data = snapshot.data.contact;
              // List<ModelChart> data = snapshot.data;
              print(data);
              return ListView.separated(
                padding: const EdgeInsets.all(8),
                itemCount: data.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Image.asset(
                                  "images/btn_call.png",
                                  fit: BoxFit.contain,
                                  height: 30,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    data[index].contactNumber,
                                    style:
                                        Theme.of(context).textTheme.headline3,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Image.asset(
                                  "images/btn_sms.png",
                                  fit: BoxFit.contain,
                                  height: 30,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    data[index].sms,
                                    style:
                                        Theme.of(context).textTheme.headline3,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Image.asset(
                                  "images/btn_email.png",
                                  fit: BoxFit.contain,
                                  height: 30,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    data[index].email,
                                    style:
                                        Theme.of(context).textTheme.headline3,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: GestureDetector(
                              onTap: () {
                                print("onTap called.");
                                _launchURL(data[index].facebook);
                              },
                              child: Row(
                                children: [
                                  Image.asset(
                                    "images/btn_facebook.png",
                                    fit: BoxFit.contain,
                                    height: 30,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "Pinguin FM Bali",
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: GestureDetector(
                              onTap: () {
                                print("onTap called.");
                                _launchURL(data[index].twitter);
                              },
                              child: Row(
                                children: [
                                  Image.asset(
                                    "images/btn_twitter.png",
                                    fit: BoxFit.contain,
                                    height: 30,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "Pinguinfm",
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            // By default show a loading spinner.
            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        // color: Colors.white,
        child: Container(
          decoration: BoxDecoration(
            color: Color.fromRGBO(41, 98, 255, 1.0),
          ),
          // padding: EdgeInsets.all(5),
          child: SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: GestureDetector(
                    onTap: () {
                      // Scaffold.of(context).openDrawer();
                      // final snackBar = SnackBar(content: Text('Profile saved'));
                      globalKey.currentState.openDrawer();
                      print("btn menu clicked");
                    },
                    child: Image.asset(
                      "images/btn_menu.png",
                      fit: BoxFit.contain,
                      height: 35,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: GestureDetector(
                    onTap: () {
                      // get data and assign to model class
                      playingStatus();
                      FlutterRadio.playOrPause(url: streamUrl);
                      // playingStatus();
                      print("btn play clicked ${streamUrl}");
                    },
                    child: Image.asset(
                      // "images/btn_play.png",
                      !isPlaying
                          ? "images/btn_play.png"
                          : "images/btn_pause.png",
                      fit: BoxFit.contain,
                      height: 55,
                    ),
                  ),
                  // child: Container(),
                ),
              ],
            ),
          ),
        ),
      ),
      drawer: drawer(context),
    );
  }

  Future<ModelContact> _getData() {
    print(" get data contact using http");
    servicewrapper wrapper = new servicewrapper();
    // print("rest get contact ${wrapper.getContact()}");
    return wrapper.getContact();
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceWebView: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchSMS() async {
    final Uri params = Uri(
      scheme: 'mailto',
      path: 'email@example.com',
    );

    var url = params.toString();
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future playingStatus() async {
    bool isP = await FlutterRadio.isPlaying();
    setState(() {
      isPlaying = isP;
      print('is playing $isPlaying');
    });
  }
}
