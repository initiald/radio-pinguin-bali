const menus = ['PLAYER', 'LIVE CHARTS', 'CONTACT', 'GALLERY', 'EVENTS', 'NEWS'];
const bg_body = '41, 121, 255, 1.0';
const bg_app_bar = '41, 98, 255, 1.0';
const url_slider = 'https://radiopinguinfm.com/images/banner/';
const url_logo = 'https://radiopinguinfm.com/images/logo.png';
const url_instagram = 'https://www.instagram.com/pinguinfmbali/';
const url_facebook = 'https://www.facebook.com/pinguinfm.bali';
const url_twitter = 'https://twitter.com/@Pinguinfm';
const url_icon_twitter =
    'https://radioarbali.com/images/icon/icon_twitter_64.png';
const url_icon_facebook =
    'https://radioarbali.com/images/icon/icon_facebook_64.png';
const url_icon_instagram =
    'https://radioarbali.com/images/icon/icon_instagram_64.png';
